package question3;

import java.sql.SQLException;

import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.Random;


public class Question3ui  extends Application{

	//initiazing all necessary parts of the ui
	Text betWorth= new Text("");
	TextField betInput =new TextField("");
	Text betType = new Text("");
	
	Button button1 = new Button("");
	Button button2 = new Button("");

	Text message= new Text("");

    Text currentMoney= new Text("");
	Text currentMoneyMessage= new Text("");

	
	//creates the ui and adds the necessary on action events and text
    public void start(Stage stage) {

    	
		

		GridPane pane = new GridPane();
		



		this.betWorth = new Text("How mucht money would you like to bet?");
		this.betInput =new TextField("");
		this.betType = new Text("Would you like to bet on even or odd?");
		
		this.button1 = new Button("Even");
		this.button2 = new Button("Odd");

		this.currentMoneyMessage= new Text("You current score is: ");
		this.currentMoney= new Text("250");

 		

		
		pane.add(this.betWorth, 0, 0);
		pane.add(this.betInput, 1, 0);
		
		pane.add(this.betType, 0, 1);
		pane.add(this.button1, 0, 2);
		pane.add(this.button2, 1, 2);
		
		pane.add(this.message, 0, 4);
		pane.add(this.currentMoneyMessage, 0, 5);
		pane.add(this.currentMoney, 1, 5);


		
		
			pane.setHgap(10);
			pane.setVgap(10);
			
			



	 
		    pane.setPadding(new Insets(10, 10, 10, 10)); 

		    pane.setAlignment(Pos.CENTER); 
 			Scene scene1 = new Scene(pane, 500, 500); 

 			stage.setScene(scene1);
 			stage.show(); 
 			
 			
			this.button1.setOnAction(new EventHandler() {
		 		
				public void handle(Event arg0) {
					
					evenBet();
				}
 			});
			
			this.button2.setOnAction(new EventHandler() {
		 		
				public void handle(Event arg0) {
					
					oddBet();
				}
 			});
			
			
			
			
			
			
			
			
	
			

			
   }

	
	
	//sees if the user won the bet if they chose even
	//adds or removes points if they won or lost and updates the text fields

	public void evenBet() {
		
		int answer = rollDice();
		int currentScore=Integer.parseInt(this.currentMoney.getText());
		
		if(!validateAmountBet()) {
			
			this.message.setText("Invalid bet. Try again.");
			
		}
		else {
		if(answer%2==0) {
			
			
			this.message.setText(" You bet "+this.betInput.getText()+" points on even and won!");
			this.currentMoney.setText(currentScore+Integer.parseInt(this.betInput.getText())+"");
		}
		
		if(answer%2!=0) {
			
			this.message.setText(" You bet "+this.betInput.getText()+" points on even and lost...");
			this.currentMoney.setText(currentScore-Integer.parseInt(this.betInput.getText())+"");
			
		}
		
		}

	}
	
	//sees if the user won the bet if they chose odd 
	//adds or removes points if they won or lost and updates the text fields

	public void oddBet() {
		
		int answer = rollDice();
		int currentScore=Integer.parseInt(this.currentMoney.getText());
		
		if(!validateAmountBet()) {
			
			this.message.setText("Invalid bet. Try again.");
			
		}
		else {
		if(answer%2!=0) {
			
			
			this.message.setText(" You bet "+this.betInput.getText()+" points on odd and won!");
			this.currentMoney.setText(currentScore+Integer.parseInt(this.betInput.getText())+"");
		}
		
		if(answer%2==0) {
			
			this.message.setText(" You bet "+this.betInput.getText()+" points on odd and lost...");
			this.currentMoney.setText(currentScore-Integer.parseInt(this.betInput.getText())+"");
			
		}
		
		}
		
	}
	
	//choses a random int betweent 1 and 6 representing the dice roll and returns it
	
	public int rollDice() {
		
		
		Random rand=new Random();
		
		
		int answer=rand.nextInt(6);
		
		return answer+1;
		
	}
	
	
	//validates the amount bet(if the number is positive or 0 and if its actually a number)
	public boolean validateAmountBet() {
		
	
	    if (this.betInput.getText() == null) {
	        return false;
	    }
	    try {
	        int number = Integer.parseInt(this.betInput.getText());
	    } catch (NumberFormatException nfe) {
	        return false;
	    }

	    if(Integer.parseInt(this.betInput.getText())>Integer.parseInt(this.currentMoney.getText())){
	    	return false;
	    	
	    }
	    
	    if(Integer.parseInt(this.betInput.getText())<0) {
	    	return false;
	    }
	        
	    return true;
	}
		
	
	
	//launches the application
	
    public static void main(String[] args) {
        Application.launch(args);
  
    }
	
	
	
}
